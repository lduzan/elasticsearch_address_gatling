name := "elastic_search_load_test"

version := "1.0"

scalaVersion := "2.11.8"

enablePlugins(GatlingPlugin)

libraryDependencies += "io.gatling" % "gatling-test-framework" % "2.2.4"
libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts" % "2.2.4"