import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.util.Random
import io.gatling.core.session.{ Expression, Session }
import scala.concurrent.duration._

object AddressSearch {
  val index = "pole_addresses"
  val mapping = "address"
  val searchEndpointUrl = s"$index/$mapping/_search"

  val pauseBetweenEachKeyStroke = 200 milliseconds
  val minCharBeforeCompletion = 1
  val maxCharCompletion = 7
  val completionStep = 1


  val townFeeder = csv("town.csv").random.circular
  val streetFeeder = csv("street.csv").random.circular
  val streetNumberFeeder = Iterator.continually(Map("streetNumber" -> (Random.nextInt(10000))))

  val computeAddress: Expression[Session] = session =>
    for (
      town <- session("town").validate[String];
      streetNumber <- session("streetNumber").validate[String];
      street <- session("street").validate[String]
    ) yield session.set("address", s"$streetNumber $street $town")

  def addressAutoComplete(size: Int) =
    exec(session =>
      session("address")
        .validate[String]
        .map {address => session.set("truncatedAddress", address.take(size))}
    )
      .exec(
        http("research")
          .get(searchEndpointUrl)
          .body(StringBody(
            """{
                 "stored_fields": "prettyAddress",
                 "size": 5,
                 "query": {
                   "match": {"fullAddress.completion": "${truncatedAddress}"}
                 }
               }"""
          ))
      )
      .pause(pauseBetweenEachKeyStroke)

  val addressSearch =
    feed(streetNumberFeeder)
      .feed(streetFeeder)
      .feed(townFeeder)
      .exec(computeAddress)
      .exec(Iterator.range(minCharBeforeCompletion, maxCharCompletion, completionStep).map(numChar =>
        addressAutoComplete(numChar)
      ))
      .exec(
        http("research")
          .get(searchEndpointUrl)
          .body(StringBody(
            """{
                 "stored_fields": ["prettyAddress"],
                  "query": {
                    "multi_match": {
                      "query": "${address}",
                      "fields": ["fullAddress^4", "fullAddress.ngram", "fullAddress.num"],
                      "cutoff_frequency": 0.07,
                      "type": "most_fields"
                    }
                  }
               }"""
          ))
      )
}
