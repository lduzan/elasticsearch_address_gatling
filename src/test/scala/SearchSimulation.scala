import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class SearchSimulation extends Simulation {

  val httpConf  = http
    .baseURL("http://10.96.20.78:9200/")

  val scn = scenario("a user that only search").exec(AddressSearch.addressSearch)

  setUp(
    scn.inject(
      rampUsersPerSec(0) to 10 during(20 second),
      constantUsersPerSec(10) during (5 minutes)
    )
  ).protocols(httpConf)
}
